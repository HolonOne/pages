---
layout: post
title:  "Holarchy in a nutshell"
author: HolonOne
date:   2021-07-17 12:00:00 +0200
categories: introduction
---
# Holarchy
## Introduction
Most time in history of mankind we assumed the earth is flat. What a cognition when we first realized it’s a sphere. That changed our world view fundamentally, like the discovery of a new dimension, opening new ways to think, speak and act. Looking back we could say that reality was always right, but our perception of reality was not.

I believe that our current perception in computer science and programming is equally wrong and that reality has an additional dimension, that will change the way we think, once discovered. I have the strong conviction that Holarchy and the concept of the Holon will have this effect to computer sciences.

*HolonOne, 2021*

## Outline
`Holon` is a [*Hash*](https://en.wikipedia.org/wiki/Associative_array) of size 1 with tremendous `Capabilities` to build and support multiple `Perspective`s on hierarchical structures, overcomming the imaginary separation of developer and user by lowering the need for a specialists translation and simultaneously raising the adoption rate for own creations. It’s designed to store and process information with fun.

`Holarchy` proclaims *Holon oriented programming* (HOP) as the evolution of current development priciples providing the `Noosphere` as a breading ground for an ever growing hierarchy extend and the only place to juggle information. All this is embedded into different technological environments by the help of `Achievements`.

When holons contain other holons they constitute a hierarchy. A hierarchy can be of arbitrary size. From the holons perspective, there is one superholon and some subholons and that makes a perfect setup for collaboration on object level. Superholons are the ones you forward unresolved request to, while subholons deliver parts of the result. A very different development approach.

Holarchy is a Ruby gem.

## Capabilities
The capabilities empower each and every holon. Here is a short overview of existing and future capabilities that empower each and every holon.

### Convertible
Holons are of one of three natures. 

|Nature |Description                      | Comparable to | 
|:--- 	  |:----                            |:-----:        | 
|SINGLE | can hold a single value         | any object    |
|CROWD  | a collection of unnamed objects | Array         |
|TRIBE  | a collection of named objects   | Hash          |

Convertible manages the automatic conversion of holons nature, if the current nature can‘t hold the results.

With the nature TRIBE, holons can hold infinite levels of subholons to build large hierarchies, one of the major benefits of Holarchy.

### Alterable
Alterable takes care of changes made to a holon, to maintain the relation to its respective superholon (see Perspectives).

### Delegable
Enables access to subholons via dot notation and delegates unresolved method invocation in the context of a holon to its superholon.

### Shapeable
While objects have their constructor and destructor, holons provide an additional level of initialization: an activator and a deactivator. Activation prepares a holon for usage while deactivation puts a holon in a kind of suspended mode where it releases all its resources.

### Findable
Select one or more subholons based on a given filter. Since a single holon can contain plenty of information these are the means to find what you‘re looking for.

### Taggable
Enables the tagging of a holon. Used i.e. for Shapeable to mark a holon, if it is in the activated state or not.

### Comparable
Makes holons comparable while respecting their nature.

### Absorbable
Absorb some data into the holon irrespective of the source (file, web service, network resource) or format (xml, csv, json, yaml). Now even complexe data is contained in the hierarchy of a single (root) holon.

### Computable
Calculate the value of a holon based on formulas like in a spreadsheet. Now you can change the behavior of the app by editing formulas and rearrange holons within the hiearchy. You don‘t need to change code anymore.  

### Inspectable
Create an intuitive readable representation of a holon.

### Displayable
Creates a HTML representation of the holon. Navigate through complex holons with your browser.

### Testable
Provides some methods the query the state of the holon.

### Distributable
Splits a holon into twins, that can run on different machines, while maintaining features and connectivity. Nice approach to scalability and consolidation of distributed holons into one holon containing all of them.

### Persistable
Automatically stores and restores your holons, the full hierarchy and state if you want to.

### Editable
The counterpart to capability displayable, so you can edit your holons in a browser. That‘s where every user can start from in a browser and create a hierarchical spread sheet as an initial starting point.

### Connectable
Links other holons as subholons in the perspective of the holon itself to overcome the restrictions of a hierarchical relation to build a full mesh. 

## Perspectives
With perspectives Holarchy tracks the relation of a holon to its superholon. You can easily change the perspective to put a holon in another context.

## Noosphere
The noosphere is the starting point of your application. It contains the achievements, that provide a link to the techological environment and of course all of your holons. And it handles the event loop.

## Achievement
An achievement is some kind of service your application relies on. Achievements can be used by every holon through specific protocols.

### WebServer
Take a web server, that displays your holons as a web page and allows you to navigate through your holarchy. You can choose different web server, but usage and invocation stays the same, so just make your choice.

### Database
If you want to make your holons persistable, you‘ll need a database to store them. The database can be of simple file based storage or a database of your choice.

### Canvas
When you like to output graphics, choose a canvas the draw on or create sprites to move.

To be continued…
