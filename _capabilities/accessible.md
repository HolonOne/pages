---
name:  "Accessible"
category: "Value"
description: "Access attributes of the holon and its sub holons"
maturity: "solution"
---
# Capability Accessible
This capability provides means to access the key and value of the holon and gain access to sub holons of the holon.

## Methods
`Accessible` has a lot of methods that can be divided into two groups.

### Holon related methods
With these methods you gain access to the holons key and value itself.

| Method           | Description                 |
|------------------|-----------------------------|
| `key`            | read the key of the holon   |
| `key=(key)`      | set the key of the holon    |
| `value(inspect)` | read the value of the holon |

<br/>

### Sub holon related methods
These methods allow access to the sub holons of a holon.

<br/>
#### Key related methods

| Method                | Description                                                       |
|-----------------------|-------------------------------------------------------------------|
| `get(key)`            | grab a sub holon by key                                           |
| `key?(key)`           | true if a sub holon with key exists                               |
| `none?(key)`          | true if no sub holon with key exists                              |
| `keys`                | return the keys of all sub holons                                 |
| `next_key`            | return the next free integer key of all sub holons                |

<br/>
#### Value related methods

| Method                | Description                                                       |
|-----------------------|-------------------------------------------------------------------|
| `[](key)`             | return the value of a sub holon with key                          |
| `get_value(key)`      | return the value of a sub holon with key                          |
| `value_of(key)`       | return the value of a sub holon with key                          |
| `fetch(key, default)` | return the value of a sub holon with key or a given default value |

<br/>

## Examples

```ruby
system = Tribe.new solar_system: {
  star: 'Sun',
  planets: %w(Mercury Venus Earth Mars Jupiter Saturn Uranus Neptune),
  dwarf_planets: %w(Ceres Orcus Pluto Haumea Quaoar Makemake Gonggong Eris Sedna)
}
system.key              # => :solar_system

system.key = :enliven_system
system.key              # => :enliven_system)
system.value            # => [star:Sun, «(planets:[Mercury, …, Neptune]), «(dwarf_planets:[Ceres, …, Sedna])]
system.value(true)      # => {:star => star:Sun, :planets => «(planets:[Mercury, …, Neptune]), :dwarf_planets => «(dwarf_planets:[Ceres, …, Sedna])}

system.get(:star)       # => ‹(star:Sun)
system.get(:planets)    # => «(planets:[Mercury, …, Neptune])

system.key?(:planets)   # => true
system.none?(:earth)    # => true
system.keys             # => [:star, :planets, :dwarf_planets]
```
