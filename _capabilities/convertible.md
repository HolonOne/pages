---
name:  "Convertible"
category: "Core"
description: "Enable a Holon to change dynamically its nature"
maturity: "tool"
---
# Capability Convertible
The capability `Convertible` enables a Holon to change its nature from SINGLE to CROWD or TRIBE or from CROWD to TRIBE.
If a dynamic nature is not wanted, it can be set as a fixed nature without the possibility to change it afterwards.

    #   :SINGLE:  The value is anything but a collection
    #   :CROWD:   The value is an ordered collection of arbitrary elements
    #   :TRIBE:   The value is a dictionary with named elements (only holons)
    # Depending on the value you assign to a holon its nature might change
    # automatically to support minimal data loss.
    # The Holon comes along with the fixed nature classes Single, Crowd and
    # Tribe, which are like a Holon except they are hard-bitten when it comes
    # to a change in nature.

