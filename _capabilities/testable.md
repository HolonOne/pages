---
name:  "Testable"
category: "Value"
description: "Enable simple tests on holons properties"
maturity: "solution"
---
# Capability Testable
A holon can be of 
- **variable** nature (class `Holon`) or 
- **fixed** nature (sub classes `Single`, `Crowd` and `Tribe`)

as documented in the capability [Convertible](convertible.md).
With the capability `Testable` a holon can be tested concerning its nature or its current state.

## Methods
All methods of the capability `Testable` return `true` if the following description is correct. Otherwise they return
`false`.

| Method | Description                                                                                          |
| ------ |------------------------------------------------------------------------------------------------------|
| `single?`     | nature of the holon is SINGLE                                                                        |
| `crowd?`      | nature of the holon is CROWD                                                                         |
| `tribe?`      | nature of the holon is TRIBE                                                                         |
| `fixed?`      | nature of the holon is fixed (default for `Single`, `Crowd` and `Tribe` classes and derived classes) |
| `empty?`      | the holon is empty                                                                                   |
| `collection?` | the holon is a collection (true for natures `CROWD` and `TRIBE`)                                     |

<br/>

## Examples
Test an uninitialized holon, which is an empty holon of nature `SINGLE`.

```ruby
holon = Holon.new

holon.single?       # => true
holon.fixed?        # => false
holon.empty?        # => true
holon.collection?   # => false
```

Then test a fixed holon, i.e. a `Crowd`, for its state.
```ruby
holon = Crowd.new [1, 2, 3]

holon.crowd?        # => true
holon.fixed?        # => true
holon.empty?        # => false
holon.collection?   # => true
```
