---
name:  "Inspectable"
category: "UI"
description: "Enables an easy to read representation of a holon via inspect"
maturity: "solution"
---
# Capability Inspectable
The capability `Inspectable` makes holons easily readable and understandable.
It targets the shortest possible representation.
Compare rubys standard notation of the objects `inspect` method with this capability and you'll see the difference.

## Methods

The class method `to_string` does all the job using the `Shodo` class to build the output.
All other methods like `to_s`and `inspect` just rely on this method.

| Method                                                | Description                                               |
|-------------------------------------------------------|-----------------------------------------------------------|
| `Holon.to_string(object, detail_level, indent_level)` | Converts any holon into a readable string representation  |
| `to_s(indent, depth)`                                 | Converts this holon into a readable string representation |
| `inspect`                                             | Inspect this holon                                        |


## Examples
```ruby
circle = Holon.new my_circle: { x: 0, y: 0, radius: 10 }

# without Inspectable
p circle  # => #<Holarchy2::Holon:0x00007fe204bd51d0 @key=:my_circle, @nature=:_tribe, 
          #       @value={:x=>#<Holarchy::Holon:0x00007fe204bd4d20 @key=:x, @nature=:_single, 
          #       @value=0>, :y=>#<Holarchy::Holon:0x00007fe204bd4820 @key=:y, @nature=:_single,
          #       @value=0>, :radius=>#<Holarchy::Holon:0x00007fe204bd4348 @key=:radius, 
          #       @nature=:_single, @value=10>}>

# with Inspectable
p circle  # => ≈(my_circle:[x:0,y:0,radius:10])
```

The difference is over 320 characters compared to 32.
This is more then ten times less characters to read.
And even with more complex holons with sub holons are shown as expected.

```ruby
work = Holon.new(address: {name: 'Donald', surname: 'Duck', 
                           street: 'Duck Gait', city: 'Duckburg', 
                           state: 'Calisota', country: 'United States of America' })
p work    # => ≈(address:[
          #      name:Donald,
          #      surname:Duck,
          #      street:Duck Gait,
          #      city:Duckburg,
          #      state:Calisota,
          #      country:United States of America
          #    ])
```