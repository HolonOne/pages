---
name:  "Taggable"
category: "Relate"
description: "Tag a holon with a symbol"
maturity: "solution"
---
# Capability Taggable
Capability `Taggable` enables you to set, query and remove tags on a holon.
A tag is a simple `Symbol` that is attached to a holon.
The capability [`Shapeable`](shapeable.md) uses tagging to mark if a holon is shaped or not.

## Methods

| Method                   | Description                                                                                                                |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `Holon.tag_container?`   | test if the holon already has a container for tags (see [minimal footprint principle](../principles/minimal_footprint.md)) |
| `tag(name)`              | tag the holon                                                                                                              |
| `untag(name)`            | remove the tag from the holon                                                                                              |
| `tag?(name)`             | test if the holon has a specific tag                                                                                       |
| `tags`                   | request all tags set on the holon                                                                                          |

<br/>

## Examples

```ruby
holon = Holon.new

holon.tag :gorgeous
holon.tag? :gorgeous    # => true
holon.tags              # => [:gorgeous]

holon.untag :gorgeous
holon.tag? :gorgeous    # => false
holon.tags              # => []
```
