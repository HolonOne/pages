---
name:  "Delegable"
category: "Core"
description: "Enables dot notation to access subholons and forwards unresolved invocations to its super holon"
maturity: "solution"
---
# Capability Delegable
Delegation is an important feature of the Holon. It introduces the dot notation and hereby enables to

1. Access a sub holon by name
2. Delegate predefined methods to the native value object of the holon
3. Delegate unresolved invocations to the super holon
4. (optional) Caching for long root chains (the number of super holons to get to the uppermost root holon)

Let's look at each of them by example.

## Access sub holons by name
You can use the dot notation as a more intuitive way to access sub holons in contrast to Holon#get.

```ruby
circle = Holon.new my_circle: { x: 0, y: 0, radius: 10 }

# standard notation
circle.get :radius          # => ‹(radius:10)
circle.get_value :radius    # => 10

# dot notation
circle.radius               # => ‹(radius:10)
circle.radius.value         # => 10
```

## Allow method delegation to the value
A holon of nature CROWD owns a value of class Array. Some methods of the Array class make perfect sense for a CROWD
holon, i.e. Array#push, Array#pop, Array#first or Array#last.

Instead of implementing them as stub routines for the Holon class the capability `Delegable` allows to forward them to
the native value of the Holon.

```ruby
nephews = Crowd.new :nephews
%i[huey dewey louie].each { |e| nephews.push e }

nephews.value               # => [:huey, :dewey, :louie]
nephews.first               # => :huey
nephews.last                # => :louie
```

## Delegate to super holon
If a holon can't resolve a request via dot notation - because there is no such sub holon in the current scope - then the
request is simply forwarded to its super holon.

```ruby
donald = Holon.new donald: { name: 'Donald' }
Holon.new(duck: { surname: 'Duck' }) { add donald }

donald.name.value           # => 'Donald'
donald.surname.value        # => 'Duck'
```

And if the super holon can't resolve it either, it is passed to the super holon of the super holon and so on until it
gets to the root holon. The root holon is the only Holon in a hierarchy that has no super holon.

![Capability Delegable](/pages/images/delegable.png)<!-- @IGNORE PREVIOUS: link -->

This is an extremely cool capability. Think of setting global configuration values in the root holon. Instantly every  
holon in the hierarchy has access to these values. And if you want to assign another value for the same configuration  
solely for a specific branch of the hierarchy, just set this value in the first holon of the branch to the preferred  
value.

Another example comes from the taxonomy of live
```ruby
spec = Holon.new species: 'Homo sapiens'
lvl1 = Holon.new(genus: 'Homo') { add spec }
lvl2 = Holon.new(tribe: 'Hominini') { add lvl1 }
lvl3 = Holon.new(subfamily: 'Hominidae') { add lvl2 }
lvl4 = Holon.new(family: 'Hominidae') { add lvl3 }
lvl5 = Holon.new(infraorder: 'Simiiformes') { add lvl4 }
lvl6 = Holon.new(suborder: 'Haplorhini') { add lvl5 }
lvl7 = Holon.new(order: 'Primates') { add lvl6 }
lvl8 = Holon.new(klass: 'Mammalia') { add lvl7 }
lvl9 = Holon.new(phylum: 'Chordata') { add lvl8 }
root = Holon.new(kingdom: 'Animalia') { add lvl9 }

spec.genus.value            # => 'Homo'
spec.tribe.value            # => 'Hominini'
spec.subfamily.value        # => 'Hominidae'
spec.family.value           # => 'Hominidae'
spec.infraorder.value       # => 'Simiiformes'
spec.suborder.value         # => 'Haplorhini'
spec.order.value            # => 'Primates'
spec.klass.value            # => 'Mammalia'
spec.phylum.value           # => 'Chordata'
spec.kingdom.value          # => 'Animalia'
```

## Enabling caching
Every time you call `spec.kingdom.value` Delegable runs through the 10 levels of the taxonomy to find `:kingdom` in
the root holon. In situations, where you often require this information traversing 10 levels up can degrade performance.

Extending the taxonomy of live example we can force the lookup of the kingdom to happen only once by creating our own  
`Species` class and declaring `:kingdom` as static.

```ruby
class Species < Holon
  static :kingdom
end

spec = Species.new species: 'Homo sapiens'
lvl1 = Holon.new(genus: 'Homo') { add spec }
# …
lvl9 = Holon.new(phylum: 'Chordata') { add lvl1 }
root = Holon.new(kingdom: 'Animalia') { add lvl9 }

spec.kingdom.value          # => 'Animalia'

# now change to original value
root.set kingdom: 'Animals'
root.kingdom.value          # => 'Animals'

# static has cached to original value
spec.kingdom.value          # => 'Animalia'
```
