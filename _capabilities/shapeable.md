---
name:  "Shapeable"
category: "Core"
description: "Introduce an extra object activation level in addition to constructor and destructor"
maturity: "solution"
---
# Capability Shapeable
## Introduction
The capability `Shapeable` introduces an extra object activation level, that enables you to control cpu and memory usage of each single holon.
Think of a holon solely containing the id of a record of some database.
This takes less memory and does not stress the cpu with needless database operations.

At the time you need to know the properties of the record, you'll invoke the method `shape` to instructs the holon to get in full bloom and load the properties from the database.
Its counterpart method `blur` instructs the holon to relax and enter some kind of suspend mode, where it releases all resources it acquired during `shape`.
It's like an on/off switch on object level. Like the transition from idea to manifestation.

These methods take care of all containing sub holons by propagating the invocation automatically down the hierarchy.
So if you `shape` your holon, all sub holons are also shaped.
A shaped holon is [tagged](taggable) as `:shaped` to avoid duplicate invocations.
With `shape` and `blur` you can easily activate solely these regions of the hierarchy you are currently working with.

If you like to customize the behaviour of `shape` and `blur` you can define one or all of the following hook methods:

| Hook         | Description                                                                                                                          |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------|
| `pre_shape`  | is invoked *before* the sub holons are shaped, <br>i.e. for creating information the shaping of the sub holons rely on               |
| `post_shape` | is invoked *after* the sub holons were shaped, <br>i.e. for processing information based on the results of the shaping of sub holons |
| `pre_blur`   | is invoked *before* the sub holons are blurred, <br>i.e. to to save or export data before it gets deleted                            |
| `post_blur`  | is invoked *after* the sub holons were blurred, <br>i.e. to delete the rest of the holon                                             |

<br>

If you don't want to shape the sub holons, because all necessary steps were made in the `pre_shape` method, then you can tag the holon as shaped like the `shape` method would do.
In this case the shaping of all sub holons and the `post_shape` method will be skipped.

```ruby
def pre_shape
  # do something
  tag SHAPED
end
```
The same works when blurring the holon:
```ruby
def pre_blur
  # do something
  untag SHAPED
end
```

Here is an example with the Fibonacci series, that starts with 0 and 1 and each successive number is calculated by
adding the two previous numbers.

```ruby
  class Fibonacci < Tribe
  DEFAULT_LIMIT = 200

  def pre_shape
    generate_numbers_upto(self[:limit] || DEFAULT_LIMIT)
  end

  def pre_blur
    number.clear
  end

  def generate_numbers_upto(limit)
    add number: [0, 1]
    loop do
      next_number = number[-2] + number[-1]
      break if next_number > limit
      number.add next_number
    end
  end
end

fib = Fibonacci.new(:fib).set(limit: 1000)    # => ≈(Fibonacci fib:[limit:1000])
fib.shape # => ≈(Fibonacci fib:[limit:1000,«(number:[0,1,1,…,377,610,987])])
fib.blur  # => ≈(Fibonacci fib:[limit:1000,«(number:)])
fib.shape # => ≈(Fibonacci fib:[limit:1000,«(number:[0,1,1,…,377,610,987])])
fib.blur  # => ≈(Fibonacci fib:[limit:1000,«(number:)])
```