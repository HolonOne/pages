---
name:  "Measurable"
category: "Value"
description: "Measure the dimensions of a holon"
maturity: "solution"
---
# Capability Measurable
When it comes to the dimensions of a holon this capability gives you all the answers.
The method `size` measures a single holon while methods `deepness` and `spread` are intended for a hierarchy.
`deepness` counts the number of vertical layers of the hierarchy while `spread` reflects the number of leaf in the hierarchy.

But why do we use *deepness* and *spread* instead of *depth* and *width*?  
Because *depth* and *width* are quite common in use and we don't want to interfere with sub holons with these names.

## Methods
All methods of the capability `Measurable` return an integer value.

| Method     | Description                                                                  |
|------------|------------------------------------------------------------------------------|
| `size`     | the number of elements contained in the holon itself                         |
| `deepness` | the number of vertical hierarchy levels contained in the holon               |
| `spread`   | the number of leaf of the hierarchy (leaf as defined by 'has no sub holons') |

<br/>

## Examples
Let's measure the world as an example.
Here the world is a hierarchy consisting of the layers `world > continent > country > capital`.

For `size` we now see:
```ruby
world  = get_world  # part of spec/test_data.rb
africa = world.get('Africa')

# number of continents
world.size          # => 7

# number of countries in africa
africa.size         # => 59
```

Method `deepness` is also straight forward.
```ruby
seychelles = africa.get('Seychelles')
victoria   = seychelles.capital

world.deepness      # => 3
africa.deepness     # => 2
seychelles.deepness # => 1
victoria.deepness   # => 0
```

And method `spread` counts the capitals.
```ruby
# number of capitals
world.spread        # => 271

# number of capitals in africa
africa.spread       # => 59

# number of capitals in Seychelles
seychelles.spread   # => 1
```
