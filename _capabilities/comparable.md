---
name:  "Comparable"
category: "Value"
description: "Enables nature independent comparison"
maturity: "solution"
---
# Capability Comparable
The capability `Comparable` enables nature independent comparison by introducing the `<=>` method for the holon. 
Herewith is defined that a `Single` is smaller than a `Crowd` and a `Crowd` is smaller than a `Tribe`.

If both holons are of same nature than the key is compared. If these are also equal, than the value will make the decision.
A comparison of a holon with another object will return nil.

## Methods
`Comparable` supports only some core methods everything else is built on.

| Method                            | Description                                                                                              |
|-----------------------------------|----------------------------------------------------------------------------------------------------------|
| `Holon.compare_keys(key1, key2)`  | compares two keys ignore case and class differences                                                      |
| `Holon.keys_equal?(key1, key2)`   | returns true if keys are equal ignoring case and class differences                                       |
| `<=>(other)`                      | compares two holons considering its nature, key and value attributes ignoring case and class differences |

## Examples

```ruby
manual   = Holon.new 'Actor' => 'Cate Blanchett'
database = Holon.new actor: 'Cate Blanchett'

manual <=> database     # => 0
```

