---
name:  "Alterable"
category: "Value"
description: "Provides CRUD to maintain the hierarchy relations"
maturity: "solution"
---
# Capability Alterable
The capability `Alterable` is not the one you would use very often directly.
But it just does a lot of work in the background while you're playing around with holons.

When you add or delete holons in the hierarchy, Holarchy has to take care of their two-way relationship.
This capability is responsible for handling changes respecting the nature of the holon in a meaningful way.

This is done by provision of the methods `set`, `add` and `del`.
These methods itself rely on atomic methods for each nature (`set_single`, `add_single`, `del_single`,`set_crowd`, `add_crowd`, `del_crowd`,`set_tribe`, `add_tribe`, `del_tribe`).
Any changes made on a holon is routed through these methods.

## Methods

### Utility methods
These methods are mainly for internal use for the capability.

| Method                         | Description                                |
|--------------------------------|--------------------------------------------|
| `Holon.friendly_nature(value)` | identify the best nature for a given value |
| `Holon.holonize(object)`       | convert an object into holon contents      |
| `Holon.extract_holons(value)`  | extract the holons from a given value      |

&nbsp;
### Methods to alter a holon
Here come the [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) methods to create, update and delete holons. 
The read aspect of CRUD is realized with the capability [Accessible](/capability/accessible).

| Method                   | Description                                              |
|--------------------------|----------------------------------------------------------|
| `clear`                  | clear the value of the holon                             |
| `init(key, value)`       | create a holon with given key and value                  |
| `set_nature(key, value)` | set the nature of the holon based on given key and value |
| `set_key(key)`           | set the key of the holon                                 |
| `key=(key)`              | alias to `set_key(key)`                                  |
| `set(value)`             | set the value of the holon                               |
| `add(value)`             | add a value to the holon (see examples!)                 |
| `delete(value)`          | delete a (part of the) value of the holon                |
| `del(value)`             | alias for `delete(value)`                                |

&nbsp;
### Nature sensitive CRUD methods

| Method                             | Description                                                                |
|------------------------------------|----------------------------------------------------------------------------|
| `stateful_context_change(&_block)` | the central change authority that keeps an eye on the hierarchy relations  |
| `set_tribe(val)`                   | set a value on a tribe holon                                               |
| `add_tribe(val)`                   | add a value on a tribe holon                                               |
| `delete_tribe(val)`                | delete a value on a tribe holon                                            |
| `set_crowd(val)`                   | set a value on a crowd holon                                               |
| `add_crowd(val)`                   | add a value on a crowd holon                                               |
| `delete_crowd(val)`                | delete a value on a crowd holon                                            |
| `set_single(val)`                  | set a value on a single holon                                              |
| `add_single(val)`                  | add a value on a single holon                                              |
| `delete_single(val)`               | delete a value on a single holon                                           |

&nbsp;
## Examples
In this example we add 1 to a holon. Depending on the holons value and nature it can have quite different outcomes.

```ruby
# add 1 to a number
number = Holon.new a: 1
number.add 1              # => ‹(a:2)

# add 1 to a text
text = Holon.new a: 'text'
text.add 1                # => ‹(a:text1)

# add 1 to a crowd
crowd = Holon.new b: [0] 
crowd.add 1               # => «(b:[0,1])

# add a single to a single
holon = Holon.new a: 1
holon.add Holon.new(b: 2) # => ≈(a:[a:1,b:2])
```