---
name:  "Serializable"
category: "IO"
description: "Convert a holon into a byte stream and vice versa"
maturity: "spark"
---
# Capability Serializable
The capability `Serializable` converts a holon into a byte stream for later recovery.