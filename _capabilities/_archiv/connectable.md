---
name:  "Connectable"
category: "Relate"
description: "Link your holons across the hierarchy"
maturity: "spark"
---
# Capability Connectable