---
name:  "Absorbable"
category: "IO"
description: "Support import from file or network sources for various data formats."
maturity: "tool"
---
# Capability Absorbable
Capability `Absorbable` enables you to import arbitrary data into a single holon.
Supported file formats to load data from are `CSV`, `JSON`, `XML`, `YAML`, `HTML` (experimental) and even `M3U` 
(playlists). Additional formats can easily be adapted.

Data can be read from a valid URI, so that local and remote files are both possible. See the examples for easy 
processing of REST-APIs.

## Methods

| Method                     | Description                    |
|----------------------------|--------------------------------|
| `absorb(uri, options = {}) | absorb data from an URI source |

## Classes

| Class              | Description                                                        |
|--------------------|--------------------------------------------------------------------|
| Absorbable::Source | Provide StringIO for URI schemes `HTTP`, `HTTPS`, `FTP` and `FILE` |

## Examples

```ruby
require 'holarchy'
include Holarchy

API_KEY = '47a4…9468'

puts "\nFrom which direction and with what speed is the wind currently blowing at my location?\n"

ipa = Holon.new.absorb 'http://ip-api.com/xml/', mime_type: 'application/xml'

par = {
  units: 'metric',
  lon: ipa.root.query[:lon],
  lat: ipa.root.query[:lat],
  appid: API_KEY
}
url = "http://api.openweathermap.org/data/2.5/weather?%s" % par.to_a.map { |e| e.join('=') }.join('&')

wea = Holon.new.absorb(url, mime_type: 'application/json')

puts " -> The wind is blowing from #{wea.wind[:deg]} degrees with a speed of #{wea.wind[:speed]} m/s."
```

```
From which direction and with what speed is the wind currently blowing at my location?
 -> The wind is blowing from 240 degrees with a speed of 8.23 m/s.
```
