---
name:  "Distributable"
category: "Relate"
description: "Spread your hierarchy in your network"
maturity: "spark"
---
# Capability Distributable
Split a holon into twins and distribute them in your network to offload parts of your hierarchy.