---
name:  "Computable"
category: "Core"
description: "Provides formulas to calculate the holons value"
maturity: "tool"
---
# Capability Computable