---
name:  "Persistable"
category: "IO"
description: "Enable persistence for a holon with its complete hierarchy"
maturity: "spark"
---
# Capability Persistable
Holons you have in your memory are easily stored and reloaded for later use.