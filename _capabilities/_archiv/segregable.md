---
name:  "Segregable"
category: "IO"
description: "Support export to file or network sources for various data formats."
maturity: "spark"
---
# Capability Segregable

