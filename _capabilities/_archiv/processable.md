---
name:  "Processable"
category: "Core"
description: "Provide means to create living action"
maturity: "spark"
---
# Capability Processable
This capability enables a holon to change its appearance and behaviour based on the contained workflow.
A Morph holon inspects its environment and acts on it with every pulse.