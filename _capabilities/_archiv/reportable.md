---
name:  "Reportable"
category: "IO"
description: "Output a hierarchy as a report"
maturity: "spark"
---
# Capability Reportable
This capability enables hierarchies that confirm to spezial policies to be printed as a report.