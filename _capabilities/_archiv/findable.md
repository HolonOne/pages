---
name:  "Findable"
category: "Core"
description: "Find sub holons based on filter criteria"
maturity: "tool"
---
# Capability Findable
With the capability `Findable` you can select sub holons based on a given filter.
You don't need to create a Filter object by yourself, `find` does this for you, when you supply the parameters.
The filter expects an BooleanCluster as depicted below.

## Filter
This is the parameter for the `find` method. The `find` method creates a Filter object from these parameters and will check all sub holons for a match.

![FindFilter.png](/pages/images/FindFilter.png)<!-- @IGNORE PREVIOUS: link -->

#### BooleanCluster
A BooleanCluster is either a condition or a condition list.

![BooleanCluster.png](/pages/images/BooleanCluster.png)<!-- @IGNORE PREVIOUS: link -->

#### Condition
Each condition is expected to be either `true` or `false`.
A condition is either a function invoked in a given context or a logical operation.

![Condition.png](/pages/images/Condition.png)<!-- @IGNORE PREVIOUS: link -->

#### ContextFunction

![ContextFunction.png](/pages/images/ContextFunction.png)<!-- @IGNORE PREVIOUS: link -->
#### LogicalOperation
![LogicalOperation.png](/pages/images/LogicalOperation.png)<!-- @IGNORE PREVIOUS: link -->


