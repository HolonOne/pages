---
name:  "Displayable"
category: "UI"
description: "Convert your hierarchy into a web page"
maturity: "toy"
---
# Capability Displayable
Each and every holon can be shown in a web page with all of its sub holons.
You can navigate easily through your hierarchy.