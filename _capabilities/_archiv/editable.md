---
name:  "Editable"
category: "UI"
description: "Create, update and delete your holons in a browser"
maturity: "spark"
---
# Capability Editable
