---
title: 'No instance variables principle'
short: 'You are hiding information from the user'
layout: principle
---
You should not use instance variables on the `Holon` class or its derivates, because you would hide information from the user.
Just store these values as a sub holon to support the fusion of developer and user.