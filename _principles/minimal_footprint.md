---
title: 'Minimal footprint principle'
short: "Don't let the holon get too large"
layout: principle
---
The holon has plenty of capabilities that make it a versatile tool for your development.
Part of the intention of `Holarchy` is that even more capabilities will be accrued later on.
In this case one should take care not to overload the holon as a base class for all objects.
This would definitely have an impact on the complete `Holarchy`.

The capability [`Taggable`](../capabilities/taggable.md), for example, uses an instance variable to store the tags attached to a holon.
`Taggable` depends on the instance variable `@tags`, but this variable is not created before it's really necessary.
If this instance variable would be created during object initialization, it would be part of each and every holon ever created irrespectively of the use of tags.
That's too much of a ballast. The `Holon` should not exceed the instance variables `@key`, `@value` and `@nature` unless it is really useful for all holons.

If you rely on temporary instance variables, that are only set when needed, you can still query the existence with `Object.instance_variable_defined?` and react as if it is unset.

